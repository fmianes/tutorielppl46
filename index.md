# PPL 46

Dernière mise à jour le : 28.09.24

## Écrire un article

- **Survoler** Contenu / Ajouter du contenu > Cliquer sur Article (site DSDEN).
- Ajouter un titre.
- Ajouter un résumé.
- Image de couverture (facultatif).
- Catégories (facultatif) mais recommandé.
- Mots-clés de remontées (facultatif) mais très recommandé.
- Contenu : ajouter une section puis un module (à voir selon les cas).
- Permission d’accès : à choisir. Si privé, identifiants académiques requis pour la consultation,
- Affichage de l’article (selon les cas) :
    - Ajouter à l'actualité "en ce moment”,
    - Afficher sur le slider en page d'accueil,
    - Apparaît dans le calendrier.
- Cocher ☑ Published (ou décocher si publication ultérieure).
- Enregistrer (ou Preview avant enregistrement).

> Les articles peuvent être écrits à l'avance pour être ensuite associés à un composant (rubrique).

## Associer un article à un composant (rubrique)

Pré-requis : L’article a déjà été écrit. Il peut comporter un média qui servira d’image d’illustration.

Procédure :

- Accéder au composant.
- Cliquer sur [Edit].
- Dans « Contenu », se placer sur un emplacement de la section et cliquer sur ➕.
- Choisir « Insérer un article ».
- Dans « ARTICLE À RÉFÉRENCER », saisir les premières lettres du titre de l’article à insérer afin de le retrouver.
- Dans Mode d’affichage, sélectionner 🔘 Couverture et décocher ⬜ Baliser le bloc ?.
- Cliquer sur [Enregistrer].

## Fil d'Ariane

Cette procédure permet de créer un fil d’Ariane **complet** qui rendra la navigation plus rapide pour les visiteurs.
Un clic permettra d'accéder directement à l'une de ces pages.

Exemple :
<ins>ACCUEIL</ins>/<ins>PÉDAGOGIE</ins>/<ins>DOSSIERS DÉPARTEMENTAUX</ins>/<ins>NUMÉRIQUE ÉDUCATIF</ins>/<ins>RESSOURCES ÉDUCATIVES</ins>/ÉDUCAJOU


**Cette procédure se fait à la fin de la rédaction d’un article.**

- Écrire son article.
- Se rendre dans la section « **État** ».
- Cliquer sur [Alias d’URL].
- Décocher : ⬜ Générer automatiquement un alias d’URL.
- Saisir alors : /pedagogie/dossiers-departementaux/le-nom-de-votre-dossier/le-nom-de-votre-autre-dossier-si-necessaire/le-titre-de-votre-article
- Par exemple, pour l’article qui se nomme « educajou », on saisira :/pedagogie/dossiers-departementaux/numerique-educatif/ressources-educatives/educajou
- Cliquer sur [ENREGISTRER].

**❗Rédaction du fil d'Ariane **: 
- ne pas mettre d’accent,
- les mots doivent être séparés par un tiret.
      

## Lien vers une ressource extérieure

Copier l'URL de la ressource.

Dans le texte de l'article :

- sélectionner la partie du texte qui sera cliquable,
- cliquer sur l'icône lien 🔗,
- dans "URL du lien" coller l'URL de la ressource et valider avec la coche verte.
- Enregistrer
- Enregistrer

## Évènement dans le calendrier

- Écrire un article.
- Cocher : Apparaît dans le calendrier.
- Saisir la date de l'évènement.
- Dans le champs "Choisissez la circonscription concernée" saisir la circonscription ou calendrier départemental.
- Enregistrer.

Un évènement d'une circo peut, si besoin, remonter dans le calendrier départemental.


## Lien vers un document sur Nuage

1 - Dans Nuage :

- cocher la case en face du document,
- cliquer sur l'icône de partage,
- en face du lien de partage cliquer sur ➕.

>Le partage **est créé** et **le lien est copié**.

Le principe est le même pour partager un dossier. Son contenu sera partagé. Le configurer en lecture seule.

2 - Dans le Portail :

- ajouter un composant Texte à votre article,
- saisir le texte,
- sélectionner la partie du texte qui sera cliquable,
- cliquer sur l'icône lien 🔗,
- dans URL coller (ctrl v) votre lien de partage créé dans Nuage,
- Enregistrer.

> Pour insérer un lien vers un document se trouvant sur internet il suffit de copier ce lien et de le coller dans l'article.  
::: 
On fera un lien direct vers un document plutôt que de l'ajouter en pièce jointe à un article.
:::
----
Outil libre et gratuit créé par Cédric Eyssette
Adapté par F. Mianes